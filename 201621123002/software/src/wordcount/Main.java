package wordcount;

import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        
        FileRead fileRead = new FileRead();
        WordCount count=fileRead.readTxtFile("test\\02.txt");
        if(count==null){
            System.out.println("文件里的数据为空");
        }
        else{
            List<Map.Entry<String,String>> list=count.sortMap();
            System.out.println("characters:"+count.getCharacters());
            System.out.println("words:"+count.getWords());
            System.out.println("lines:"+count.getLines());
            if(list.size()<10){
                for(int i=0;i<list.size();i++){
                    System.out.println("<"+list.get(i).getKey()+">:"+list.get(i).getValue());
                }
            }
            else{
                for(int i=0;i<10;i++){
                    System.out.println("<"+list.get(i).getKey()+">:"+list.get(i).getValue());
                }
            }
            
        }
    }
}