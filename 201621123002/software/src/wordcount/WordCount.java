package wordcount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;


public class WordCount {

    private int characters;
    private int words;
    private int lines;
    Map<String, String> dic = new TreeMap<String, String>();

    public WordCount() {
        this.characters = 0;
        this.words = 0;
        this.lines = 0;
    }

    public void Mapsort() {

    }

    public void setDic(Map<String, String> dic) {
        this.dic = dic;
    }

    public int getCharacters() {
        return characters;
    }

    public void setCharacters(int characters) {
        this.characters = characters;
    }

    public int getWords() {
        return words;
    }

    public void setWords(int words) {
        this.words = words;
    }

    public int getLines() {
        return lines;
    }

    public void setLines(int lines) {
        this.lines = lines;
    }
    
    

    public List<Map.Entry<String, String>> sortMap() {
        List<Map.Entry<String, String>> list = new ArrayList<Map.Entry<String, String>>(this.dic.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, String>>() {
            public int compare(Entry<String, String> o1, Entry<String, String> o2) {
                int value1=Integer.parseInt(o1.getValue());
                int value2=Integer.parseInt(o2.getValue());
                if(value1==value2){
                    return 0;
                }
                else{
                    return value2-value1;
                }
            }
        });
        if(list.size()==0){
            return null;
        }else{
            return list;
        }
    }

}
