package wordcount;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;


public class FileRead {

    public FileRead() {
    }

    public WordCount readTxtFile(String filePath) {
        WordCount count = new WordCount();
        Map<String, String> dic = new TreeMap<String, String>();
        int characters = 0;
        int words = 0;
        int lines = 0;
        int line = 0;
        String s = "!@#$%^&*().|,\t? \\";
        String str = "";
        ArrayList<String> array = new ArrayList<>();
        try {
            String encoding = "UTF-8";
            File file = new File(filePath);
            if (file.isFile() && file.exists()) {
                InputStreamReader read = new InputStreamReader(
                        new FileInputStream(file), encoding);
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                while ((lineTxt = bufferedReader.readLine()) != null) {
                    lines++;
                    characters = characters + lineTxt.length();
                    str = "";
                    if (lineTxt.length() == 0) {
                        line++;
                    }
                    for (int i = 0; i < lineTxt.length(); i++) {
                        if (s.indexOf(lineTxt.charAt(i)) >= 0) {
                            if (str.length() >= 4) {
                                if (array.contains(str.toLowerCase())) {
                                    int value = Integer.parseInt(dic.get(str.toLowerCase())) + 1;
                                    dic.put(str.toLowerCase(), "" + value);
                                    str="";
                                } else {
                                    array.add(str.toLowerCase());
                                    dic.put(str.toLowerCase(), "" + 1);
                                    str="";
                                    words++;
                                }
                            } else {
                                str = "";
                            }
                        }
                        if ((lineTxt.charAt(i) >= 'a' && lineTxt.charAt(i) <= 'z') || (lineTxt.charAt(i) >= 'A' && lineTxt.charAt(i) <= 'Z')) {
                            str = str + lineTxt.charAt(i);
                        }
                        if (lineTxt.charAt(i) >= '0' && lineTxt.charAt(i) <= '9') {
                            if (str.length() >= 4) {
                                str = str + lineTxt.charAt(i);
                            } else {
                                str = "";
                            }
                        }
                        
                    }
                }
                read.close();
                count.setCharacters(characters + lines-1);
                count.setLines(lines - line);
                count.setWords(words);
                count.setDic(dic);
            } else {
                System.out.println("找不到指定的文件");
            }
        } catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
        }
        if(characters==0)
            return null;
        else
            return count;
    }
}
